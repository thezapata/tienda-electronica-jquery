
$("#tipoRep").change(function(){
  var tipoRep = $("#tipoRep").val();
   $("#valor").val(tipoRep);
    });

$("#calcular").click(function(){
  var costoArticulo = $("#valor").val();
  var cantidad = $("#cantidad").val();
  var recargo= $("input[name='recargo']");
  var porcentaje = 0;
  var subTotal = 0;
  var totalRecargo = 0;
  var total=0;
  for(var i=0; i<recargo.length;i++ ){
    if(recargo[i].checked){
      porcentaje = recargo[i].value;
    }
  }
  subTotal = costoArticulo*cantidad;
  totalRecargo = subTotal*porcentaje/100;
  total = subTotal - totalRecargo;
  
  $("#subtotal").val(subTotal);
  $("#totalRec").val(totalRecargo);
  $("#total").val(total);
});

